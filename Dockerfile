FROM ubuntu:bionic
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV TOOLCHAIN_DIR="/arm/gcc-arm-none-eabi/bin"

ADD https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2 /arm/toolchain.tar.bz2

RUN apt-get update \
    && apt-get install --yes \
        wget \
        git \
        ssh \
        zip \
        unzip \
        curl \
        cmake \
        g++ \
        software-properties-common \
        lsb-release \
        bzip2 \
        libsdl2-dev \
        mesa-utils \
        libgl1-mesa-glx \
        x11-apps \
        ccache \
        gdb \
        python \
        cowsay \
# PIP
    && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python get-pip.py \
    && rm get-pip.py \
# cppclean
    && pip install --upgrade cppclean \
# ccache
    && mkdir -p /.ccache \
    && chmod 0777 /.ccache \
# ARM
    && cd /arm && tar xvjf /arm/toolchain.tar.bz2 \
    && ls /arm \
    && mv /arm/gcc-arm-* /arm/gcc-arm-none-eabi \
    && chmod -R 0777 /arm \
# LLVM
    && wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && ./llvm.sh 11 \
    && apt-get install clang-format-11 \
    && printf '#!/bin/bash\nclang-format-11 "$@"\n' > /usr/bin/clang-format \
    && chmod +x /usr/bin/clang-format \
# Cleanup
    && rm -rf /var/lib/apt/lists/*
